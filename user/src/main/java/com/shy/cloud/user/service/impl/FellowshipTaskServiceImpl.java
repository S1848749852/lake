package com.shy.cloud.user.service.impl;

import com.shy.cloud.user.dao.AdminbaseDao;
import com.shy.cloud.user.dao.PeopleTaskRelationDao;
import com.shy.cloud.user.dao.UserTaskRelationDao;
import com.shy.cloud.user.entity.Adminbase;
import com.shy.cloud.user.entity.FellowshipTask;
import com.shy.cloud.user.dao.FellowshipTaskDao;
import com.shy.cloud.user.entity.PeopleTaskRelation;
import com.shy.cloud.user.entity.UserTaskRelation;
import com.shy.cloud.user.service.AdminbaseService;
import com.shy.cloud.user.service.FellowshipTaskService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * fellowship_task(FellowshipTask)表服务实现类
 *
 * @author shy
 * @since 2023-03-10 09:34:05
 */
@Service("fellowshipTaskService")
public class FellowshipTaskServiceImpl implements FellowshipTaskService {
    @Resource
    private FellowshipTaskDao fellowshipTaskDao;

    @Resource
    private UserTaskRelationDao userTaskRelationDao;

    @Resource
    private PeopleTaskRelationDao peopleTaskRelationDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public FellowshipTask queryById(Integer id) {
        return this.fellowshipTaskDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param fellowshipTask 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<FellowshipTask> queryByPage(FellowshipTask fellowshipTask, PageRequest pageRequest) {
        long total = this.fellowshipTaskDao.count(fellowshipTask);
        return new PageImpl<>(this.fellowshipTaskDao.queryAllByLimit(fellowshipTask, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param fellowshipTask 实例对象
     * @return 实例对象
     */
    @Override
    public FellowshipTask insert(FellowshipTask fellowshipTask) {
        this.fellowshipTaskDao.insert(fellowshipTask);
        return fellowshipTask;
    }

    /**
     * 修改数据
     *
     * @param fellowshipTask 实例对象
     * @return 实例对象
     */
    @Override
    public FellowshipTask update(FellowshipTask fellowshipTask) {
        this.fellowshipTaskDao.update(fellowshipTask);
        return this.queryById(fellowshipTask.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.fellowshipTaskDao.deleteById(id) > 0;
    }

    @Override
    public List<FellowshipTask> queryFellowshipTaskAllByAdminId(Integer adminId) {
        return fellowshipTaskDao.queryFellowshipTaskAllByAdminId(adminId);
    }

    /**
     * 新增联谊任务
     *
     * @param fellowshipTasks 实体集合
     * @return 返回list集合
     */
    @Override
    @Transactional
    public String addFellowshipTaskAllByAdminId(List<FellowshipTask> fellowshipTasks) {
        int flag=0;
        for (FellowshipTask fellowshipTask : fellowshipTasks) {
            //批量插入
            fellowshipTaskDao.insert(fellowshipTask);
            //进行组织人员关系分配插入
            String[] ids = fellowshipTask.getDepartmentGroupId().split(",");
            for (String id : ids) {
                UserTaskRelation userTaskRelation=new UserTaskRelation();
                userTaskRelation.setAdminid(Integer.valueOf(id));
                userTaskRelation.setTaskid(fellowshipTask.getId());
                userTaskRelationDao.insert(userTaskRelation);
            }
            //进行家庭人员关系分配插入
            String[] peopleIds = fellowshipTask.getPeopleGroupId().split(",");
            for (String peopleId : peopleIds) {
                PeopleTaskRelation peopleTaskRelation=new PeopleTaskRelation();
                peopleTaskRelation.setId(Integer.valueOf(peopleId));
                peopleTaskRelation.setTaskId(fellowshipTask.getId());
                peopleTaskRelationDao.insert(peopleTaskRelation);
            }
            flag++;
        }
        return "成功新增"+flag+"条";
    }

    /**
     * 我待处理(已处理)的联谊任务查询
     *
     * @param adminId 主键
     * @return 返回list集合
     */
    @Override
    public List<FellowshipTask> queryFellowshipTaskAllToExecute(Integer adminId, String status) {
        return fellowshipTaskDao.queryFellowshipTaskAllToExecute(adminId,status);
    }
}
