package com.shy.cloud.user.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
//可刷新范围
@RefreshScope
public class TestController {
    @Value("${server.port}")
    private String port;

//    //此配置在配置中心，平常注释，需要时需要启动配置中心
//    @Value("${test}")
//    private String test;

    @GetMapping("/hello")
    public String hello(@RequestParam("id") Integer id){
        System.out.println(port);
        return "hello world"+id;
    }

//    @GetMapping("/test")
//    public String test(){
//        return test;
//    }
}
