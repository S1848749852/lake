package com.shy.cloud.user.dao;

import com.shy.cloud.user.entity.FellowshipSynergy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * fellowship_synergy(FellowshipSynergy)表数据库访问层
 *
 * @author shy
 * @since 2023-03-09 15:20:55
 */

public interface FellowshipSynergyDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FellowshipSynergy queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<FellowshipSynergy> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param fellowshipSynergy 实例对象
     * @return 对象列表
     */
    List<FellowshipSynergy> queryAll(FellowshipSynergy fellowshipSynergy);

    /**
     * 新增数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 影响行数
     */
    int insert(FellowshipSynergy fellowshipSynergy);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<FellowshipSynergy> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<FellowshipSynergy> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<FellowshipSynergy> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<FellowshipSynergy> entities);

    /**
     * 修改数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 影响行数
     */
    int update(FellowshipSynergy fellowshipSynergy);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);


    List<FellowshipSynergy> queryFellAll(@Param("adminId") Integer adminId);
}