package com.shy.cloud.user.controller;

import com.shy.cloud.user.common.CommonResult;
import com.shy.cloud.user.entity.Adminbase;
import com.shy.cloud.user.service.AdminbaseService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Adminbase)表控制层
 *
 * @author shy
 * @since 2023-03-09 16:41:25
 */
@RestController
@RequestMapping("/adminbase")
public class AdminbaseController {
    /**
     * 服务对象
     */
    @Resource
    private AdminbaseService adminbaseService;

    /**
     * 登录后查询本人信息
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Adminbase selectOne(Integer id) {
        return this.adminbaseService.queryById(id);
    }

    /**
     * 部门分组查询
     *
     * @return 返回list集合
     */
    @GetMapping("/queryDepartmentGroup")
    public CommonResult queryDepartmentGroup() {
        List<Adminbase> departments=adminbaseService.queryDepartmentGroup();
        return CommonResult.success(departments);
    }


}