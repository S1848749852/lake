package com.shy.cloud.user.entity;

import java.io.Serializable;

/**
 * (Peoplebase)实体类
 *
 * @author shy
 * @since 2023-03-10 11:35:47
 */
public class Peoplebase implements Serializable {
    private static final long serialVersionUID = -57433324234242832L;
    
    private Integer id;
    
    private Integer adminId;
    
    private Integer userid;
    
    private String name;
    
    private String phone;
    
    private String sex;
    
    private String department;
    
    private String address;
    
    private String loginStatus;
    
    private String time;
    
    private String loginArrive;
    
    private String createTime;
    
    private String createName;
    
    private String isDeleted;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLoginArrive() {
        return loginArrive;
    }

    public void setLoginArrive(String loginArrive) {
        this.loginArrive = loginArrive;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

}

