package com.shy.cloud.user.entity;

import java.io.Serializable;

/**
 * fellowship_task(FellowshipTask)实体类
 *
 * @author shy
 * @since 2023-03-10 09:34:05
 */
public class FellowshipTask implements Serializable {
    private static final long serialVersionUID = 631365869966664934L;
    
    private Integer id;
    
    private Integer adminid;
    /**
     * ?û?id
     */
    private Integer userid;
    
    private String name;
    
    private Integer peoId;
    
    private String content;
    
    private String status;
    
    private String startTime;
    
    private String endTime;
    
    private String type;
    
    private String address;

    private String num;

    private String departmentGroup;

    private String departmentGroupId;

    private String peopleGroupId;

    public String getPeopleGroupId() {
        return peopleGroupId;
    }

    public void setPeopleGroupId(String peopleGroupId) {
        this.peopleGroupId = peopleGroupId;
    }

    public String getDepartmentGroupId() {
        return departmentGroupId;
    }

    public void setDepartmentGroupId(String departmentGroupId) {
        this.departmentGroupId = departmentGroupId;
    }


    public String getDepartmentGroup() {
        return departmentGroup;
    }

    public void setDepartmentGroup(String departmentGroup) {
        this.departmentGroup = departmentGroup;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminid() {
        return adminid;
    }

    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPeoId() {
        return peoId;
    }

    public void setPeoId(Integer peoId) {
        this.peoId = peoId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

}

