package com.shy.cloud.user.controller;

import com.shy.cloud.user.common.CommonResult;
import com.shy.cloud.user.entity.FellowshipTask;
import com.shy.cloud.user.service.FellowshipTaskService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * fellowship_task(FellowshipTask)表控制层
 *
 * @author shy
 * @since 2023-03-10 09:34:04
 */
@RestController
@RequestMapping("/fellowshipTask")
public class FellowshipTaskController {
    /**
     * 服务对象
     */
    @Resource
    private FellowshipTaskService fellowshipTaskService;

    /**
     * 分页查询
     *
     * @param fellowshipTask 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<FellowshipTask>> queryByPage(FellowshipTask fellowshipTask, PageRequest pageRequest) {
        return ResponseEntity.ok(this.fellowshipTaskService.queryByPage(fellowshipTask, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<FellowshipTask> queryById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.fellowshipTaskService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param fellowshipTask 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<FellowshipTask> add(FellowshipTask fellowshipTask) {
        return ResponseEntity.ok(this.fellowshipTaskService.insert(fellowshipTask));
    }

    /**
     * 编辑数据
     *
     * @param fellowshipTask 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<FellowshipTask> edit(FellowshipTask fellowshipTask) {
        return ResponseEntity.ok(this.fellowshipTaskService.update(fellowshipTask));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.fellowshipTaskService.deleteById(id));
    }

    /**
     * 我待处理(已处理)的联谊任务查询
     *
     * @param adminId 主键
     * @return 返回list集合
     */
    @PostMapping("/queryFellowshipTaskAllToExecute")
    public CommonResult queryFellowshipTaskAllToExecute(@RequestParam("adminId") Integer adminId,
                                                        @RequestParam("status") String status) {
        List<FellowshipTask> fellowshipTasks=fellowshipTaskService.queryFellowshipTaskAllToExecute(adminId,status);
        return CommonResult.success(fellowshipTasks);
    }

    /**
     * 我发布的联谊任务查询
     *
     * @param adminId 主键
     * @return 返回list集合
     */
    @PostMapping("/queryFellowshipTaskAllByAdminId")
    public CommonResult queryFellowshipTaskAllByAdminId(@RequestParam("adminId") Integer adminId) {
        List<FellowshipTask> fellowshipTasks=fellowshipTaskService.queryFellowshipTaskAllByAdminId(adminId);
        return CommonResult.success(fellowshipTasks);
    }

    /**
     * 新增联谊任务
     *
     * @param fellowshipTasks 实体集合
     * @return 返回list集合
     */
    @PostMapping("/addFellowshipTaskAllByAdminId")
    public CommonResult addFellowshipTaskAllByAdminId(@RequestBody  List<FellowshipTask> fellowshipTasks) {
        String result=fellowshipTaskService.addFellowshipTaskAllByAdminId(fellowshipTasks);
        return CommonResult.success(result);
    }
}

