package com.shy.cloud.user.service.impl;

import com.shy.cloud.user.entity.Peoplebase;
import com.shy.cloud.user.dao.PeoplebaseDao;
import com.shy.cloud.user.service.PeoplebaseService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * (Peoplebase)表服务实现类
 *
 * @author shy
 * @since 2023-03-10 11:35:47
 */
@Service("peoplebaseService")
public class PeoplebaseServiceImpl implements PeoplebaseService {
    @Resource
    private PeoplebaseDao peoplebaseDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Peoplebase queryById(Integer id) {
        return this.peoplebaseDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param peoplebase 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<Peoplebase> queryByPage(Peoplebase peoplebase, PageRequest pageRequest) {
        long total = this.peoplebaseDao.count(peoplebase);
        return new PageImpl<>(this.peoplebaseDao.queryAllByLimit(peoplebase, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param peoplebase 实例对象
     * @return 实例对象
     */
    @Override
    public Peoplebase insert(Peoplebase peoplebase) {
        this.peoplebaseDao.insert(peoplebase);
        return peoplebase;
    }

    /**
     * 修改数据
     *
     * @param peoplebase 实例对象
     * @return 实例对象
     */
    @Override
    public Peoplebase update(Peoplebase peoplebase) {
        this.peoplebaseDao.update(peoplebase);
        return this.queryById(peoplebase.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.peoplebaseDao.deleteById(id) > 0;
    }
}
