package com.shy.cloud.user.dao;

import com.shy.cloud.user.entity.UserTaskRelation;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * (UserTaskRelation)表数据库访问层
 *
 * @author shy
 * @since 2023-03-10 11:02:54
 */
public interface UserTaskRelationDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserTaskRelation queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param userTaskRelation 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<UserTaskRelation> queryAllByLimit(UserTaskRelation userTaskRelation, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param userTaskRelation 查询条件
     * @return 总行数
     */
    long count(UserTaskRelation userTaskRelation);

    /**
     * 新增数据
     *
     * @param userTaskRelation 实例对象
     * @return 影响行数
     */
    int insert(UserTaskRelation userTaskRelation);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserTaskRelation> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserTaskRelation> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserTaskRelation> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<UserTaskRelation> entities);

    /**
     * 修改数据
     *
     * @param userTaskRelation 实例对象
     * @return 影响行数
     */
    int update(UserTaskRelation userTaskRelation);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

