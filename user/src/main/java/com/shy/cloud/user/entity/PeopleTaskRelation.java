package com.shy.cloud.user.entity;

import java.io.Serializable;

/**
 * (PeopleTaskRelation)实体类
 *
 * @author shy
 * @since 2023-03-10 11:30:54
 */
public class PeopleTaskRelation implements Serializable {
    private static final long serialVersionUID = 927225072826372437L;
    
    private Integer id;
    
    private Integer taskId;
    
    private Integer peopleId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Integer peopleId) {
        this.peopleId = peopleId;
    }

}

