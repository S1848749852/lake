package com.shy.cloud.user.service.impl;

import com.shy.cloud.user.dao.FellowshipSynergyDao;
import com.shy.cloud.user.entity.FellowshipSynergy;
import com.shy.cloud.user.service.FellowshipSynergyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * fellowship_synergy(FellowshipSynergy)表服务实现类
 *
 * @author shy
 * @since 2023-03-09 15:20:58
 */
@Service("fellowshipSynergyService")
public class FellowshipSynergyServiceImpl implements FellowshipSynergyService {
    @Resource
    private FellowshipSynergyDao fellowshipSynergyDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public FellowshipSynergy queryById(Integer id) {
        return this.fellowshipSynergyDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<FellowshipSynergy> queryAllByLimit(int offset, int limit) {
        return this.fellowshipSynergyDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 实例对象
     */
    @Override
    public FellowshipSynergy insert(FellowshipSynergy fellowshipSynergy) {
        this.fellowshipSynergyDao.insert(fellowshipSynergy);
        return fellowshipSynergy;
    }

    /**
     * 修改数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 实例对象
     */
    @Override
    public FellowshipSynergy update(FellowshipSynergy fellowshipSynergy) {
        this.fellowshipSynergyDao.update(fellowshipSynergy);
        return this.queryById(fellowshipSynergy.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.fellowshipSynergyDao.deleteById(id) > 0;
    }

    /**
     * 通过身份查询协同联谊列表
     *
     * @param adminId status
     * @return 全
     */
    @Override
    public List<FellowshipSynergy> queryFellAll(Integer adminId, String status) {
        //查所有
        List<FellowshipSynergy> fellowshipSynergys = fellowshipSynergyDao.queryFellAll(adminId);
        for (FellowshipSynergy fellowshipSynergy : fellowshipSynergys) {
            //取消结束都是删除，共用一个命令词传入
            if (adminId.equals(fellowshipSynergy.getAdminId())&&("取消").equals(fellowshipSynergy.getStatus())){
                //如果是自己发布，则可以取消任务
                fellowshipSynergyDao.deleteById(fellowshipSynergy.getId());
            }
            //若是传入状态不一致，则更改报名状态
            if (fellowshipSynergy.getStatus()!=null&&!(fellowshipSynergy.getStatus().equals(status))){
                fellowshipSynergy.setStatus(status);
                fellowshipSynergyDao.update(fellowshipSynergy);
            }
        }
        return fellowshipSynergys;
    }

    @Override
    public String addFellOne(FellowshipSynergy fellowshipSynergy) {
        int insert = fellowshipSynergyDao.insert(fellowshipSynergy);
        if (insert==1){
            return "成功";
        }
        return "失败";
    }
}