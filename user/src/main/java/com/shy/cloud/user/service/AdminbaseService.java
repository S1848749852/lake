package com.shy.cloud.user.service;

import com.shy.cloud.user.entity.Adminbase;

import java.util.List;

/**
 * (Adminbase)表服务接口
 *
 * @author shy
 * @since 2023-03-09 16:41:24
 */
public interface AdminbaseService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Adminbase queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Adminbase> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param adminbase 实例对象
     * @return 实例对象
     */
    Adminbase insert(Adminbase adminbase);

    /**
     * 修改数据
     *
     * @param adminbase 实例对象
     * @return 实例对象
     */
    Adminbase update(Adminbase adminbase);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<Adminbase> queryDepartmentGroup();

}