package com.shy.cloud.user.dao;

import com.shy.cloud.user.entity.PeopleTaskRelation;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * (PeopleTaskRelation)表数据库访问层
 *
 * @author shy
 * @since 2023-03-10 11:30:54
 */
public interface PeopleTaskRelationDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    PeopleTaskRelation queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param peopleTaskRelation 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<PeopleTaskRelation> queryAllByLimit(PeopleTaskRelation peopleTaskRelation, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param peopleTaskRelation 查询条件
     * @return 总行数
     */
    long count(PeopleTaskRelation peopleTaskRelation);

    /**
     * 新增数据
     *
     * @param peopleTaskRelation 实例对象
     * @return 影响行数
     */
    int insert(PeopleTaskRelation peopleTaskRelation);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<PeopleTaskRelation> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<PeopleTaskRelation> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<PeopleTaskRelation> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<PeopleTaskRelation> entities);

    /**
     * 修改数据
     *
     * @param peopleTaskRelation 实例对象
     * @return 影响行数
     */
    int update(PeopleTaskRelation peopleTaskRelation);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

