package com.shy.cloud.user.controller;

import com.shy.cloud.user.entity.Peoplebase;
import com.shy.cloud.user.service.PeoplebaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Peoplebase)表控制层
 *
 * @author shy
 * @since 2023-03-10 11:35:47
 */
@RestController
@RequestMapping("peoplebase")
public class PeoplebaseController {
    /**
     * 服务对象
     */
    @Resource
    private PeoplebaseService peoplebaseService;

    /**
     * 分页查询
     *
     * @param peoplebase 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<Peoplebase>> queryByPage(Peoplebase peoplebase, PageRequest pageRequest) {
        return ResponseEntity.ok(this.peoplebaseService.queryByPage(peoplebase, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<Peoplebase> queryById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.peoplebaseService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param peoplebase 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Peoplebase> add(Peoplebase peoplebase) {
        return ResponseEntity.ok(this.peoplebaseService.insert(peoplebase));
    }

    /**
     * 编辑数据
     *
     * @param peoplebase 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<Peoplebase> edit(Peoplebase peoplebase) {
        return ResponseEntity.ok(this.peoplebaseService.update(peoplebase));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.peoplebaseService.deleteById(id));
    }

}

