package com.shy.cloud.user.service;

import com.shy.cloud.user.entity.FellowshipSynergy;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * fellowship_synergy(FellowshipSynergy)表服务接口
 *
 * @author shy
 * @since 2023-03-09 15:20:57
 */
@Mapper
public interface FellowshipSynergyService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FellowshipSynergy queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<FellowshipSynergy> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 实例对象
     */
    FellowshipSynergy insert(FellowshipSynergy fellowshipSynergy);

    /**
     * 修改数据
     *
     * @param fellowshipSynergy 实例对象
     * @return 实例对象
     */
    FellowshipSynergy update(FellowshipSynergy fellowshipSynergy);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 通过身份查询协同联谊列表
     *
     * @param adminId status
     * @return 全
     */
    List<FellowshipSynergy> queryFellAll(Integer adminId, String status);

    String addFellOne(FellowshipSynergy fellowshipSynergy);
}