package com.shy.cloud.user.entity;

import java.io.Serializable;

/**
 * (UserTaskRelation)实体类
 *
 * @author shy
 * @since 2023-03-10 11:03:25
 */
public class UserTaskRelation implements Serializable {
    private static final long serialVersionUID = -14434877923917599L;
    
    private Integer id;
    
    private Integer adminid;
    
    private Integer taskid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminid() {
        return adminid;
    }

    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    public Integer getTaskid() {
        return taskid;
    }

    public void setTaskid(Integer taskid) {
        this.taskid = taskid;
    }

}

