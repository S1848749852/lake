package com.shy.cloud.user.controller;

import com.shy.cloud.user.common.CommonResult;
import com.shy.cloud.user.entity.FellowshipSynergy;
import com.shy.cloud.user.service.FellowshipSynergyService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * fellowship_synergy(FellowshipSynergy)表控制层
 *
 * @author shy
 * @since 2023-03-09 15:20:59
 */
@RestController
@RequestMapping("/fellowshipSynergy")
public class FellowshipSynergyController {
    /**
     * 服务对象
     */
    @Resource
    private FellowshipSynergyService fellowshipSynergyService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public FellowshipSynergy selectOne(Integer id) {
        return this.fellowshipSynergyService.queryById(id);
    }

    /**
     * 通过身份查询协同联谊列表（有地图再加参数）
     *
     * @param fellowshipSynergy 主
     * @return 全
     */
    @PostMapping("/queryFellAll")
    public CommonResult queryFellAll(@RequestBody FellowshipSynergy fellowshipSynergy){
        List<FellowshipSynergy> fellowshipSynergys = fellowshipSynergyService.queryFellAll(fellowshipSynergy.getAdminId(),fellowshipSynergy.getStatus());
        return CommonResult.success(fellowshipSynergys);
    }

    /**
     * 新建联谊
     *
     * @param fellowshipSynergy 主
     * @return 全
     */
    @PostMapping("/addFellOne")
    public CommonResult addFellOne(@RequestBody FellowshipSynergy fellowshipSynergy){
        String result = fellowshipSynergyService.addFellOne(fellowshipSynergy);
        return CommonResult.success(result);
    }

}