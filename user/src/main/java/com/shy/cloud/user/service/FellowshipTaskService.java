package com.shy.cloud.user.service;

import com.shy.cloud.user.entity.FellowshipTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * fellowship_task(FellowshipTask)表服务接口
 *
 * @author shy
 * @since 2023-03-10 09:34:05
 */
public interface FellowshipTaskService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FellowshipTask queryById(Integer id);

    /**
     * 分页查询
     *
     * @param fellowshipTask 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    Page<FellowshipTask> queryByPage(FellowshipTask fellowshipTask, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param fellowshipTask 实例对象
     * @return 实例对象
     */
    FellowshipTask insert(FellowshipTask fellowshipTask);

    /**
     * 修改数据
     *
     * @param fellowshipTask 实例对象
     * @return 实例对象
     */
    FellowshipTask update(FellowshipTask fellowshipTask);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 联谊任务查询
     *
     * @param adminId 主键
     * @return 返回list集合
     */
    List<FellowshipTask> queryFellowshipTaskAllByAdminId(Integer adminId);

    /**
     * 新增联谊任务
     *
     * @param fellowshipTasks 实体集合
     * @return 返回list集合
     */
    String addFellowshipTaskAllByAdminId(List<FellowshipTask> fellowshipTasks);

    /**
     * 我待处理(已处理)的联谊任务查询
     *
     * @param adminId 主键
     * @return 返回list集合
     */
    List<FellowshipTask> queryFellowshipTaskAllToExecute(Integer adminId, String status);
}
