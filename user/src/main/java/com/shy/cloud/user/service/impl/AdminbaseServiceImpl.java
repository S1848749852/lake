package com.shy.cloud.user.service.impl;

import com.shy.cloud.user.dao.AdminbaseDao;
import com.shy.cloud.user.entity.Adminbase;
import com.shy.cloud.user.service.AdminbaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Adminbase)表服务实现类
 *
 * @author shy
 * @since 2023-03-09 16:41:24
 */
@Service("adminbaseService")
public class AdminbaseServiceImpl implements AdminbaseService {
    @Resource
    private AdminbaseDao adminbaseDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Adminbase queryById(Integer id) {
        return this.adminbaseDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Adminbase> queryAllByLimit(int offset, int limit) {
        return this.adminbaseDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param adminbase 实例对象
     * @return 实例对象
     */
    @Override
    public Adminbase insert(Adminbase adminbase) {
        this.adminbaseDao.insert(adminbase);
        return adminbase;
    }

    /**
     * 修改数据
     *
     * @param adminbase 实例对象
     * @return 实例对象
     */
    @Override
    public Adminbase update(Adminbase adminbase) {
        this.adminbaseDao.update(adminbase);
        return this.queryById(adminbase.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.adminbaseDao.deleteById(id) > 0;
    }

    @Override
    public List<Adminbase> queryDepartmentGroup() {
        return adminbaseDao.queryDepartmentGroup();
    }
}