package com.shy.cloud.zuul.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.shy.cloud.zuul.common.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

@Component
public class GatewayFilter extends ZuulFilter {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Value("#{'${pathlist}'.split(',')}")
    private List<String> pathlist;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        // response.setContentType(“text/html;charset=UTF-8”);
        System.out.println(pathlist);

        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletResponse response = requestContext.getResponse();
        //浏览器编码字符集
        response.setContentType("text/html;charset=UTF-8");
        HttpServletRequest request = requestContext.getRequest();
        String uri = request.getRequestURI();
        //那些路径可以直接放行
        boolean a = pathlist.stream().anyMatch(path->StringUtils.contains(uri,path));
        if (a){
            return null;
        }

        /*-----暂时关闭权限验证------*/
//        String authorization = request.getHeader("Authorization");
//        String token = StringUtils.substring(authorization,"bearer".length()).trim();
//        Set<String> auths= null;
//
//        try {
//            auths = jwtTokenUtil.getAuthsFromToken(token);
//        } catch (Exception e) {
//            // 处理token过期
//            if(e instanceof ExpiredJwtException){
//                requestContext.setResponseBody("token 过期");
//                return null;
//            }
//            e.printStackTrace();
//        }
//        //验证权限
//        boolean b = auths.stream().anyMatch(auth->StringUtils.equals(auth,uri));
//        if (!b){
//            //设置响应
//            requestContext.setResponseBody("您没有权限");
//            //禁止路由
//            requestContext.setSendZuulResponse(false);
//        }

        /*-----------*/

        //因为过期底层会自动变空，所以不用判断过期
        //boolean tokenExpired=false;
//        try {
//            tokenExpired= jwtTokenUtil.isTokenExpired(token);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        if (tokenExpired){
//            requestContext.setResponseBody("token 过期");
//        }


//        LOGGER.info("Remote host:{},method:{},uri:{}", host, method, uri);
        //String token = request.getHeader("token");

        /*-----暂时关闭权限验证------*/
        //requestContext.addZuulRequestHeader("Authorization","bearer "+token);
        return null;
    }
}

