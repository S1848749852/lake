package com.shy.cloud.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
//允许他成为一个Eureka的客户端,@EnableEurekaClient只能用到Eureka，而@EnableDiscoveryClient可以用在所有注册中心
@EnableDiscoveryClient
//开启网关代理
@EnableZuulProxy
public class ZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class,args);
    }
}
