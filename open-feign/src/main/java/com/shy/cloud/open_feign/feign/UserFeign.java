package com.shy.cloud.open_feign.feign;


import com.shy.cloud.open_feign.feign.fallback.UserFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//value填写服务名称
@FeignClient(value = "user", fallback = UserFallback.class)
public interface UserFeign {

    @GetMapping("/admin/hello")
    //FeignClient中每一个@RequestParam 都要设置value，否者报错
    //@RequestParam("id")这里加了("id")。如果你加了value属性("id"),
        // 这样会用id代替后面的id  也就是说你地址里面传入的参数名称为id　localhost:8080/list?id＝？　这种
    String hello(@RequestParam("id") Integer id);

}
