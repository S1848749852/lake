package com.shy.cloud.open_feign.controller;

import com.shy.cloud.open_feign.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feign")
public class TestFeignController {
    @Autowired
    private UserFeign userFeign;

    @GetMapping("/getHello")
    public String getHello(){
        int id=1;
        String hello = userFeign.hello(id);
        return hello;
    }
}
