package com.shy.cloud.open_feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OpenFeignApp {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignApp.class,args);
    }
}

