package com.shy.cloud.open_feign.feign.fallback;

import com.shy.cloud.open_feign.feign.UserFeign;
import org.springframework.stereotype.Component;

@Component("userFeign")
public class UserFallback implements UserFeign {
    public String hello(Integer id) {
        return "调用Admin的hello方法失败，这里是服务降级";
    }
}
