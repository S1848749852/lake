package com.shy.cloud.security.service.impl;


import com.shy.cloud.security.model.Admin;
import com.shy.cloud.security.model.AdminPermission;
import com.shy.cloud.security.service.UniversalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;


@Service("userDetailsService")
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UniversalService universalService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Admin admin= universalService.getUserByName(username);
        //根据userId 查 user的权限
        List<AdminPermission> permissionList= universalService.getPermissionsByUserId(admin.getId());
        HashSet<AdminPermission> permissions = new HashSet<>(permissionList);
        admin.setAuthorities(permissions);
        return admin;
    }
}