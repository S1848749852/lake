package com.shy.cloud.security.service;

import com.shy.cloud.security.common.api.CommonResult;
import com.shy.cloud.security.model.Admin;
import com.shy.cloud.security.model.AdminPermission;
import com.shy.cloud.security.vo.LoginParams;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UniversalService {
    Admin getUserByName(String username);

    List<AdminPermission> getPermissionsByUserId(Integer id);

    CommonResult login(LoginParams loginParams, HttpServletRequest request);

    CommonResult register(Admin admin);
}
