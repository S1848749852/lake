package com.shy.cloud.security.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginParams implements Serializable {
    private String username;
    private String password;
}