package com.shy.cloud.security.controller;

import com.shy.cloud.security.common.api.CommonResult;
import com.shy.cloud.security.model.Admin;
import com.shy.cloud.security.service.UniversalService;
import com.shy.cloud.security.vo.LoginParams;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("")
public class UniversalController {
    @Resource
    private UniversalService universalService;

    /**
     * 登录接口
     * @param loginParams
     * @return CommonResult
     */
    @PostMapping("/login")
    public CommonResult login(@RequestBody LoginParams loginParams, HttpServletRequest request){
        return universalService.login(loginParams,request);
    }

    @PostMapping("/register")
    public CommonResult register(@RequestBody Admin admin){
        return universalService.register(admin);
    }
}
