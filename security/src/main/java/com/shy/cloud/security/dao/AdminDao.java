package com.shy.cloud.security.dao;

import com.github.pagehelper.Page;
import com.shy.cloud.security.model.Admin;
import com.shy.cloud.security.model.AdminPermission;

import java.util.List;

public interface AdminDao {
    Admin getUserByName(String username);

    List<AdminPermission> getPermissionsByUserId(Integer id);

    Page<Admin> getAdminsPage(Admin admin);

    Integer getAdminsCount(Admin admin);

    Integer register(Admin admin);

    Admin selectOne(Integer id);

    Integer updateUser(Admin admin);

    Integer deleteUser(Integer id);

    List<Admin> getAllAdmins();

    Admin getAdminsRole();

    void insertAcross(String username, String groupCode, String code);
}
