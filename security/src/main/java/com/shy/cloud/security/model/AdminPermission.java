package com.shy.cloud.security.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdminPermission implements GrantedAuthority {
    private Integer id;
    private Integer pid;
    private String name;
    private String value;
    private String icon;
    private Integer type;
    private String uri;
    private Integer status;
    private Date createTime;
    private String sort;

    //z这里可以设置value，进行值的权限判断，在分布式中可以设置uri，在调用接口时进行权限判断，权限就是登陆的接口名字，必须相同
    @Override
    public String getAuthority() {
        return this.uri;
    }
}