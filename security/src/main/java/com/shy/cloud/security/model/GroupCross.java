package com.shy.cloud.security.model;

import java.io.Serializable;

/**
 * (GroupCross)实体类
 *
 * @author shy
 * @since 2020-11-26 10:36:41
 */
public class GroupCross implements Serializable {
    private static final long serialVersionUID = -21613440459150642L;

    private Integer id;

    private String username;

    private String oldGroupCode;

    private String newGroupCode;

    private Integer pageSize;

    private Integer pageNum;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldGroupCode() {
        return oldGroupCode;
    }

    public void setOldGroupCode(String oldGroupCode) {
        this.oldGroupCode = oldGroupCode;
    }

    public String getNewGroupCode() {
        return newGroupCode;
    }

    public void setNewGroupCode(String newGroupCode) {
        this.newGroupCode = newGroupCode;
    }

}