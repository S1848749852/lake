package com.shy.cloud.security.service.impl;

import com.shy.cloud.security.common.api.CommonResult;
import com.shy.cloud.security.common.utils.JwtTokenUtil;
import com.shy.cloud.security.dao.AdminDao;
import com.shy.cloud.security.model.Admin;
import com.shy.cloud.security.model.AdminPermission;
import com.shy.cloud.security.service.UniversalService;
import com.shy.cloud.security.vo.LoginParams;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service
public class UniversalServiceImpl implements UniversalService {
    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private AdminDao adminDao;

//    @Resource
//    private EsAdminService esAdminService;
    /**
     * 根据用户名查询账号实体
     */
    @Override
    public Admin getUserByName(String username) {
        Admin userByName = adminDao.getUserByName(username);
        Assert.notNull(userByName,"该账号不存在");
        return userByName;
    }

    /**
     * 根据id查询权限
     */
    @Override
    public List<AdminPermission> getPermissionsByUserId(Integer id) {
        return adminDao.getPermissionsByUserId(id);
    }

    /**
     *  登陆实现
     */
    @Override
    public CommonResult login(LoginParams loginParams, HttpServletRequest request) {
        //查询账号
        Admin userByName = getUserByName(loginParams.getUsername());
        if (userByName.getStatus()==0||userByName.getStatusNum()==3){
            return CommonResult.failed("请联系管理员解冻");
        }
        //比对密码
        boolean matches = passwordEncoder.matches(loginParams.getPassword(), userByName.getPassword());
        if (!matches){
            //第二次给他+1变成2
            if (userByName.getStatusNum()<2){
                Admin admin=new Admin();
                admin.setId(userByName.getId());
                admin.setStatusNum(userByName.getStatusNum()+1);
                adminDao.updateUser(admin);
            }else {
                //第三次变成0
                Admin admin=new Admin();
                admin.setId(userByName.getId());
                admin.setStatusNum(0);
                admin.setStatus(0);
                adminDao.updateUser(admin);
            }
            return  CommonResult.failed("账户或者密码不对");
        }

        //查询权限，放入set集合
        List<AdminPermission> permissions = getPermissionsByUserId(userByName.getId());
        HashSet<AdminPermission> adminPermissions = new HashSet<>(permissions);
        userByName.setAuthorities(adminPermissions);

        //生成token
        String token;
        try {
            token = jwtTokenUtil.generateToken(userByName);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.failed("未知错误");
        }

//        //要开启ES
//        //获取ip
//        String ipAddr = IpUtil.getIpAddr(request);
//        //调用esAdminService来讲数据写入es
//        esAdminService.importEsAdminToEs(userByName.getId(),ipAddr);

        return CommonResult.success(token);
    }

    @Override
    public CommonResult register(Admin admin) {
        //用户名已存在
        Admin userByName = adminDao.getUserByName(admin.getUsername());
        if (userByName!=null){
            return CommonResult.failed("用户名重复");
        }
        String password=passwordEncoder.encode(admin.getPassword());
        admin.setPassword(password);
        admin.setDeleted("f");
        admin.setCreateTime(new Date());
        admin.setStatusNum(0);
        Integer a=adminDao.register(admin);
        return CommonResult.success(a);
    }
}
