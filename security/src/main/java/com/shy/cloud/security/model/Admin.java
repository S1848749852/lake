package com.shy.cloud.security.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * 后台用户表(Admin)实体类
 *
 * 重写实现了UserDetails
 * @author 沈昊宇
 * @since 2020-11-20 10:32:33
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Admin implements UserDetails {
    private Integer id;
    private String username;
    private String password;
    private String icon;
    private String email;
    private String nickName;
    private String note;
    private Date createTime;
    private Date loginTime;
    private Integer status;
    private String statusName;
    private Integer pageSize;
    private Integer pageNum;
    private String deleted;
    private Integer statusNum;
    private String roleName;
    private String groupCode;
    private String groupName;
    private String code;
    private String state;
    private String arrive;
    private String time;
    private String startTime;
    private String endTime;
    private String dailyState;
    private String dailyDay;
    private String dailyPro;
    private String dailyTeacher;
    private String dailyLeader;
    private String dailyContent;
    private String dailyReward;
    private String dailyBad;
    private String dailyQuestion;
    private Set<? extends GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    //每个下面加set，因为redis读取的时候要读取set，而没有会报错
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    public void setAccountNonExpired(boolean accountNonExpired) {
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    public void setAccountNonLocked(boolean accountNonLocked) {
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
    }

    @Override
    public boolean isEnabled() {
        if(this.status==null){
            return false;
        }
        return this.status==1;
    }

    public void setEnabled(boolean enabled){
    }
}