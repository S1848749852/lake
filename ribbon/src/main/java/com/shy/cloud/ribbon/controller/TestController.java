package com.shy.cloud.ribbon.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ribbon")
public class TestController {
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/getHello")
    public String getHello(){

//        //获取服务列表，可以自己写轮询
        List<ServiceInstance> admins = discoveryClient.getInstances("user");

        int id=1;
        /*
         * getForObject：
         * 返回对象为响应体中数据转化成的对象，基本上可以理解为Json
         * getForEntity：
         * 返回对象为ResponseEntity对象，包含了响应中的一些重要信息，比如响应头、响应状态码、响应体等
         */
        //地址，返回值类型，参数
        ResponseEntity<String> forEntity = restTemplate.getForEntity("http://USER/admin/hello?id="+id, String.class);

        //getForObject和getForEntity一样，这里测试的是使用map，{id}映射的map的键
        Map<String,Integer> map=new HashMap<String, Integer>();
        map.put("id",id);
        String forObject = restTemplate.getForObject("http://USER/admin/hello?id={id}", String.class, map);
        return "测试结果"+forEntity.getBody()
                +"---"+forEntity.getStatusCode()
                +"---"+forEntity.getStatusCodeValue()
                +"---"+forEntity.getHeaders()
                +"---"+forEntity.getClass()
                +"---"+forObject
                +"---"+forObject.getBytes();
    }

}
